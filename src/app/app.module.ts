import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TipoEquipoListaComponent } from './tipo-equipo-lista/tipo-equipo-lista.component';

@NgModule({
  declarations: [
    AppComponent,
    TipoEquipoListaComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
