import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoEquipoListaComponent } from './tipo-equipo-lista.component';

describe('TipoEquipoListaComponent', () => {
  let component: TipoEquipoListaComponent;
  let fixture: ComponentFixture<TipoEquipoListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoEquipoListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoEquipoListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
